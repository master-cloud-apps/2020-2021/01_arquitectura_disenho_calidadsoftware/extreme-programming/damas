[![pipeline status](https://gitlab.com/master-cloud-apps/extreme-programming/damas/badges/master/pipeline.svg)](https://gitlab.com/master-cloud-apps/extreme-programming/damas/-/commits/master)
[![coverage](https://gitlab.com/master-cloud-apps/extreme-programming/damas/badges/master/coverage.svg)](https://gitlab.com/master-cloud-apps/extreme-programming/damas/-/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=org.eyo%3Adamas&metric=alert_status)](https://sonarcloud.io/dashboard?id=org.eyo%3Adamas)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=org.eyo%3Adamas&metric=coverage)](https://sonarcloud.io/dashboard?id=org.eyo%3Adamas)

# Damas

Juego de las damas para realizar refactoring.

