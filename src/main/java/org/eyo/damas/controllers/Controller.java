package org.eyo.damas.controllers;

import org.eyo.damas.models.Color;
import org.eyo.damas.models.Coordinate;
import org.eyo.damas.models.Game;
import org.eyo.damas.models.State;

class Controller {

    protected Game game;
    protected State state;

    protected Controller(Game game, State state) {
        assert game != null;
        assert state != null;
        this.game = game;
        this.state = state;
    }

    public Color getColor(Coordinate coordinate) {
        assert coordinate != null;
        return this.game.getColor(coordinate);
    }

    public int getDimension() {
        return this.game.getDimension();
    }

}
