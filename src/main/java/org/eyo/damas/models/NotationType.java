package org.eyo.damas.models;

public enum NotationType {
    ALGEBRAIC("Algebraic"),
    DESCRIPTIVE("Descriptive"),
    MATHEMATICAL("Mathematical");

    private String type;


    NotationType(String type) {
        this.type = type;
    }
}
