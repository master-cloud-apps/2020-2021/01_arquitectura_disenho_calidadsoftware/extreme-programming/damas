package org.eyo.damas.views;

import org.eyo.damas.controllers.InteractorController;
import org.eyo.damas.models.Coordinate;
import org.eyo.damas.models.Piece;

import java.util.HashMap;
import java.util.Map;

class GameView extends SubView {

    private Map<Integer, String> columnMap;
    private Map<Integer, Integer> rowMap;

    public GameView() {
        this.columnMap = new HashMap<>();
        this.rowMap = new HashMap<>();
        this.columnMap.put(0, "a");
        this.columnMap.put(1, "b");
        this.columnMap.put(2, "c");
        this.columnMap.put(3, "d");
        this.columnMap.put(4, "e");
        this.columnMap.put(5, "f");
        this.columnMap.put(6, "g");
        this.columnMap.put(7, "h");
        this.rowMap.put(1, 8);
        this.rowMap.put(2, 7);
        this.rowMap.put(3, 6);
        this.rowMap.put(4, 5);
        this.rowMap.put(5, 4);
        this.rowMap.put(6, 3);
        this.rowMap.put(7, 2);
        this.rowMap.put(8, 1);
    }

    void write(InteractorController controller) {
        assert controller != null;
        final int DIMENSION = controller.getDimension();
        this.writeNumbersLine(DIMENSION, "numbers");
        for (int i = 0; i < DIMENSION; i++)
            this.writePiecesRow(i, controller);
        this.writeNumbersLine(DIMENSION, "letters");
    }

    private void writeNumbersLine(final int DIMENSION, String type) {
        if (type.equals("letters")) {
            this.console.write("   ");
            for (int i = 0; i < DIMENSION; i++)
                this.console.write(this.columnMap.get(i) + " ");
            this.console.writeln();
        } else {
            this.console.write("   ");
            for (int i = 0; i < DIMENSION; i++)
                this.console.write(i + 1 + " ");
            this.console.writeln();
        }
    }

    private void writePiecesRow(final int row, InteractorController controller) {
        this.console.write((row + 1) + "");
        for (int j = 0; j < controller.getDimension(); j++) {
            if (j == 0) {
                this.console.write(" ");
            }
            Piece piece = controller.getPiece(new Coordinate(row, j));
            if (piece == null)
                this.console.write("| ");
            else {
                this.console.write("|" + piece.getCode() + "");
            }

            if (j == controller.getDimension() - 1)
                this.console.write("| ");
        }
        this.console.writeln(this.rowMap.get(row + 1) + "");
    }

}
