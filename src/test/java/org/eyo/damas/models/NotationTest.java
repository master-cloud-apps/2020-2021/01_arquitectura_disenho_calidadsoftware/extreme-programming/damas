package org.eyo.damas.models;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class NotationTest {

    private Notation notation;

    @Before
    public void setUp(){
        this.notation = new Notation();
    }

    @Test
    public void check_not_null(){
        assertNotNull(this.notation);
    }

    @Test
    public void should_return_chess_notation_with_columns(){
        NotationType notationType = this.notation.getNotationType("h1");

        assertEquals(NotationType.ALGEBRAIC, notationType);
    }

    @Test
    public void should_return_descriptive_notation(){
        NotationType notationType = this.notation.getNotationType("P4CR");

        assertEquals(NotationType.DESCRIPTIVE, notationType);
    }

    @Test
    public void should_return_mathematical_notation(){
        NotationType notationType = this.notation.getNotationType("11");

        assertEquals(NotationType.MATHEMATICAL, notationType);
    }

    @Test
    public void should_calculate_mathematical_position(){
        assertEquals("88", this.notation.getMathPosition("h1"));
        assertEquals("87", this.notation.getMathPosition("g1"));
        assertEquals("13", this.notation.getMathPosition("c8"));
        assertEquals("12", this.notation.getMathPosition("b8"));
        assertEquals("11", this.notation.getMathPosition("a8"));
    }

    @Test(expected = AssertionError.class)
    public void should_raise_error_if_more_than_2_strings_passed(){
        this.notation.getMathPosition("h12");
    }

    @Test(expected = AssertionError.class)
    public void should_raise_error_if_more_algebraic_not_passed(){
        this.notation.getMathPosition("j7");
    }

    @Test(expected = AssertionError.class)
    public void should_raise_error_if_more_algebraic_not_passed_number_great(){
        this.notation.getMathPosition("a9");
    }
}
