package org.eyo.damas.models;

public class BoardTest {

    protected Game game;
    protected Game expectedGame;

    protected void setGame(Color color, String... pieces){
        this.game = new GameBuilder().setTurn(color).pieces(pieces).build();
    }

    protected void setExpectedGame(Color color, String... pieces){
        this.expectedGame = new GameBuilder().setTurn(color).pieces(pieces).build();
    }
}
