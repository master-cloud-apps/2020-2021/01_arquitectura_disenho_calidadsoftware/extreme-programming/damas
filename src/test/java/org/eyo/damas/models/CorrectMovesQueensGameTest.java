package org.eyo.damas.models;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CorrectMovesQueensGameTest extends BoardTest {

    @Test
    public void should_eat_one_black_pawn() {
        this.setGame(Color.WHITE, "f2W", "d4b");
        this.setExpectedGame(Color.BLACK, "a7W");

        this.game.move(Coordinate.getInstance("f2"), Coordinate.getInstance("a7"));

        assertEquals(this.expectedGame, this.game);
        assertTrue(this.game.isBlocked());
    }

    @Test
    public void should_eat_two_black_pawn() {
        this.setGame(Color.WHITE, "g1W", "e3b", "c5b");
        this.setExpectedGame(Color.BLACK, "b6W");

        this.game.move(
                Coordinate.getInstance("g1"),
                Coordinate.getInstance("d4"),
                Coordinate.getInstance("b6"));

        assertEquals(this.expectedGame, this.game);
        assertTrue(this.game.isBlocked());
    }

    @Test
    public void should_eat_three_black_pawn() {
        this.setGame(Color.WHITE, "g1W", "e3b", "c5b", "c7b");
        this.setExpectedGame(Color.BLACK, "d8W");

        this.game.move(
                Coordinate.getInstance("g1"),
                Coordinate.getInstance("d4"),
                Coordinate.getInstance("b6"),
                Coordinate.getInstance("d8"));

        assertEquals(this.expectedGame, this.game);
        assertTrue(this.game.isBlocked());
    }

    @Test
    public void should_eat_four_black_pawn() {
        this.setGame(Color.WHITE, "g1W", "e3b", "c5b", "c7b", "f6b");
        this.setExpectedGame(Color.BLACK, "h4W");

        this.game.move(
                Coordinate.getInstance("g1"),
                Coordinate.getInstance("d4"),
                Coordinate.getInstance("b6"),
                Coordinate.getInstance("d8"),
                Coordinate.getInstance("h4"));

        assertEquals(this.expectedGame, this.game);
        assertTrue(this.game.isBlocked());
    }

}
