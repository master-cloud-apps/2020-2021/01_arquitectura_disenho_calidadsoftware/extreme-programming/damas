package org.eyo.damas.models;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InCorrectMovesQueensGameTest extends BoardTest {

    @Test
    public void should_not_advance_too_much() {
        this.setGame(Color.WHITE, "f2W", "e3b", "d4b");
        Error error = this.game.move(
                Coordinate.getInstance("f2"),
                Coordinate.getInstance("c5"));

        assertEquals(Error.TOO_MUCH_EATINGS, error);
    }


}
