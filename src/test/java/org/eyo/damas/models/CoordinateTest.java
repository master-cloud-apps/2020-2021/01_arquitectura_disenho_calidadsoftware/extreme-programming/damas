package org.eyo.damas.models;

import org.junit.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class CoordinateTest {

    @Test
    public void should_not_create_coordinate_descriptive() {
        Coordinate coordinateDescriptive = Coordinate.getInstance("3CR");

        assertNull(coordinateDescriptive);
    }

    @Test
    public void should_create_new_coordinate_11() {
        Coordinate coordinate11 = Coordinate.getInstance("11");

        assertEquals(0, coordinate11.getRow());
        assertEquals(0, coordinate11.getColumn());
    }

    @Test
    public void should_create_new_coordinate_70_algebraic() {
        Coordinate coordinate11 = Coordinate.getInstance("a1");

        assertEquals(7, coordinate11.getRow());
        assertEquals(0, coordinate11.getColumn());
    }

    @Test
    public void should_create_new_coordinate_72_algebraic() {
        Coordinate coordinate11 = Coordinate.getInstance("b8");

        assertEquals(0, coordinate11.getRow());
        assertEquals(1, coordinate11.getColumn());
    }

    @Test(expected = AssertionError.class)
    public void should_assert_coordinate_null_in_getDirection(){
        createCoordinate("h4").getDirection(null);
    }

    @Test
    public void should_get_direction_from_coordinate_to_next() {
        assertEquals(
                Direction.SE,
                Coordinate.getInstance("f4")
                        .getDirection(Coordinate.getInstance("g5")));
        assertEquals(
                Direction.SW,
                Coordinate.getInstance("d4")
                        .getDirection(Coordinate.getInstance("c5")));
        assertEquals(
                Direction.NE,
                Coordinate.getInstance("g5")
                        .getDirection(Coordinate.getInstance("h4")));
        assertEquals(
                Direction.NW,
                Coordinate.getInstance("h8")
                        .getDirection(Coordinate.getInstance("g7")));
        assertEquals(null, Coordinate.getInstance("d4").getDirection(Coordinate.getInstance("f4")));
        assertEquals(null, Coordinate.getInstance("a1").getDirection(Coordinate.getInstance("c1")));
    }

    @ParameterizedTest
    @CsvSource({
            "g7, h8",
            "f6, h8",
            "h8, g7",
            "f4, e5",
            "b6, a5",
            "e3, d4",
            "f2, g3",
            "a1, b2",
            "b2, c3",
            "b4, a5",
    })
    public void should_check_diagonal_coordinates(String coordinate1, String coordinate2) {
        assertTrue(Coordinate.getInstance(coordinate1)
                .isOnDiagonal(Coordinate.getInstance(coordinate2)));
    }

    @ParameterizedTest
    @CsvSource({
            "h8, e6",
            "f4, d5",
            "b6, d6",
            "a3, a5",
            "f2, e7",
            "a1, f8",
            "b2, a7",
            "b4, f6",
    })
    public void should_check_wrong_diagonals(String coordinate1, String coordinate2) {
        assertFalse(Coordinate.getInstance(coordinate1)
                .isOnDiagonal(Coordinate.getInstance(coordinate2)));
    }

    @Test(expected = AssertionError.class)
    public void should_assert_between_diagonal_at_least_size_2(){
        createCoordinate("b4").
                getBetweenDiagonalCoordinate(createCoordinate("c5"));
    }

    @Test(expected = AssertionError.class)
    public void should_assert_should_be_on_diagonal(){
        createCoordinate("b4").
                getBetweenDiagonalCoordinate(createCoordinate("h5"));
    }

    @ParameterizedTest
    @CsvSource({
            "h8, g7, f6",
            "f4, e5, d6",
            "b6, c5, d4",
            "a3, b4, c5",
            "f2, e3, d4",
    })
    public void should_get_coordinate_in_diagonal_between_two(String origin, String between, String destiny) {
        assertEquals(
                Coordinate.getInstance(between),
                Coordinate.getInstance(origin).
                        getBetweenDiagonalCoordinate(Coordinate.getInstance(destiny))
        );
    }

    private static Coordinate createCoordinate(String square) {
        return Coordinate.getInstance(square);
    }

    @ParameterizedTest
    @CsvSource({
            "1, c6, b5",
            "2, b2, d4",
            "3, h8, e5",
            "4, h8, d4",
            "5, a1, f6",
            "6, h2, b8",
            "7, h1, a8",
    })
    public void should_resolve_diagonal_distance(int distance, String initialSquare, String finalSquare) {
        assertEquals(
                distance,
                createCoordinate(initialSquare)
                        .getDiagonalDistance(createCoordinate(finalSquare)));
    }

    @Test
    public void should_get_between_diagonal_coordinates_2_squares(){
        List<Coordinate> coordinates = createCoordinate("h8")
                .getBetweenDiagonalCoordinates(createCoordinate("e5"));
        assertEquals(
                Arrays.asList(createCoordinate("g7"), createCoordinate("f6")),
                coordinates
        );
    }

    @Test
    public void should_get_between_diagonal_coordinates_3_squares(){
        List<Coordinate> coordinates = createCoordinate("h1")
                .getBetweenDiagonalCoordinates(createCoordinate("d5"));
        assertEquals(
                Arrays.asList(
                        createCoordinate("g2"),
                        createCoordinate("f3"),
                        createCoordinate("e4")),
                coordinates
        );
    }

    @Test
    public void should_get_between_diagonal_coordinates_4_squares(){
        List<Coordinate> coordinates = createCoordinate("f1")
                .getBetweenDiagonalCoordinates(createCoordinate("a6"));
        assertEquals(
                Arrays.asList(
                        createCoordinate("e2"),
                        createCoordinate("d3"),
                        createCoordinate("c4"),
                        createCoordinate("b5")),
                coordinates
        );
    }

    @Test
    public void should_get_one_diagonal_coordinate_b4(){
        Coordinate b4 = createCoordinate("b4");
        List<Coordinate> oneStepCoordinates = Arrays.asList(
                createCoordinate("c3"),
                createCoordinate("c5"),
                createCoordinate("a5"),
                createCoordinate("a3")
        );
        assertEquals(oneStepCoordinates, b4.getDiagonalCoordinates(1));
    }

    @Test
    public void should_get_one_diagonal_coordinate_d4(){
        Coordinate d4 = createCoordinate("d4");
        List<Coordinate> oneStepCoordinates = Arrays.asList(
                createCoordinate("e3"),
                createCoordinate("e5"),
                createCoordinate("c5"),
                createCoordinate("c3")
        );
        assertEquals(oneStepCoordinates, d4.getDiagonalCoordinates(1));
    }

    @Test
    public void should_get_two_diagonal_coordinate_d4(){
        Coordinate d4 = createCoordinate("d4");
        List<Coordinate> oneStepCoordinates = Arrays.asList(
                createCoordinate("f2"),
                createCoordinate("f6"),
                createCoordinate("b6"),
                createCoordinate("b2")
        );
        assertEquals(oneStepCoordinates, d4.getDiagonalCoordinates(2));
    }

    @Test
    public void should_get_one_diagonal_coordinate_h5(){
        Coordinate h5 = createCoordinate("h5");
        List<Coordinate> oneStepCoordinates = Arrays.asList(
                createCoordinate("g6"),
                createCoordinate("g4")
        );
        assertEquals(oneStepCoordinates, h5.getDiagonalCoordinates(1));
    }

    @Test
    public void should_get_one_diagonal_coordinate_a1(){
        Coordinate a1 = createCoordinate("a1");
        List<Coordinate> oneStepCoordinates = Arrays.asList(
                createCoordinate("b2")
        );
        assertEquals(oneStepCoordinates, a1.getDiagonalCoordinates(1));
    }

    @Test
    public void should_get_two_diagonal_coordinate_a1(){
        Coordinate a1 = createCoordinate("a1");
        List<Coordinate> oneStepCoordinates = Arrays.asList(
                createCoordinate("c3")
        );
        assertEquals(oneStepCoordinates, a1.getDiagonalCoordinates(2));
    }


}
