package org.eyo.damas.models;

import org.eyo.damas.dtos.InitialPosition;
import org.eyo.damas.dtos.PieceInBoard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

public class GameBuilder {

    private InitialPosition initialPosition;
    private Color color;
    Map<String, Color> mapColors;
    Map<String, Piece> mapPieces;

    public GameBuilder() {
        this.mapColors = new HashMap<>();
        this.mapPieces = new HashMap<>();
        this.mapPieces.put("w", new Pawn(Color.WHITE));
        this.mapPieces.put("W", new Draught(Color.WHITE));
        this.mapPieces.put("b", new Pawn(Color.BLACK));
        this.mapPieces.put("B", new Draught(Color.BLACK));
        this.initialPosition = null;
        this.color = null;
    }

    public GameBuilder pieces(String... stringPieces) {
        List<PieceInBoard> initialPos = new ArrayList<>();
        for (String coordinatePiece : stringPieces) {
            assert Pattern.matches("[a-h][1-8][wWbB]", coordinatePiece);
            String coordinate = coordinatePiece.substring(0, 2);
            String pieceKind = coordinatePiece.substring(2, 3);
            PieceInBoard piece = new PieceInBoard(Coordinate.getInstance(coordinate), this.mapPieces.get(pieceKind));
            initialPos.add(piece);
        }
        this.initialPosition = new InitialPosition(initialPos);
        return this;
    }

    public GameBuilder setTurn(Color color) {
        this.color = color;
        return this;
    }

    public Game build() {
        Board board = new Board();
        Game game = new Game(board);
        this.setInitialTurn(game, board);
        this.setInitialPosition(board);
        return game;
    }

    private void setInitialPosition(Board board) {
        for (PieceInBoard piece : this.initialPosition.getPieces()){
            board.put(piece.getCoordinate(), piece.getKind());
        }
    }

    private void setInitialTurn(Game game, Board board) {
        if (this.color == Color.BLACK) {
            board.put(Coordinate.getInstance("d4"), new Pawn(Color.WHITE));
            game.move(Coordinate.getInstance("d4"), Coordinate.getInstance("e5"));
            board.remove(Coordinate.getInstance("e5"));
        }
    }
}
