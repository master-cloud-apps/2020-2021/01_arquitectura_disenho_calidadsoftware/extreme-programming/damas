package org.eyo.damas.models;

import org.junit.Before;
import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StateTest {

    private State state;

    @Before
    public void setUp(){
        this.state = new State();
    }

    @Test
    public void should_start_initial_state(){
        assertEquals(StateValue.INITIAL, this.state.getValueState());
    }

    @Test
    public void should_change_states(){
        this.state.next();
        assertEquals(StateValue.IN_GAME, this.state.getValueState());

        this.state.next();
        assertEquals(StateValue.FINAL, this.state.getValueState());

        this.state.next();
        assertEquals(StateValue.EXIT, this.state.getValueState());

    }

    @Test(expected = AssertionError.class)
    public void should_not_change_state_from_exit(){
        this.state.next();
        this.state.next();
        this.state.next();
        this.state.next();
    }
}
