package org.eyo.damas.views;

import org.eyo.damas.controllers.ResumeController;
import org.eyo.damas.models.Game;
import org.eyo.damas.models.State;
import org.eyo.damas.models.StateValue;
import org.eyo.damas.utils.YesNoDialog;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class ResumeViewTest {

    @InjectMocks
    private ResumeView resumeView;

    @Mock
    private YesNoDialog yesNoDialog;
    private ResumeController resumeController;
    private Game game;
    private State state;

    @BeforeEach
    public void setUp(){
        this.state = new State();
        this.game = new Game();
        this.resumeController = new ResumeController(this.game, this.state);
        this.state.next();
        this.state.next();
    }

    @Test
    public void should_end_game_when_user_said_so(){
        assertEquals(StateValue.FINAL, this.state.getValueState());

        this.resumeView.interact(this.resumeController);

        assertEquals(StateValue.EXIT, this.state.getValueState());
    }

    @Test
    public void should_restart_game_when_user_said_so(){
        assertEquals(StateValue.FINAL, this.state.getValueState());
        when(this.yesNoDialog.read(anyString())).thenReturn(true);

        this.resumeView.interact(this.resumeController);

        assertEquals(StateValue.INITIAL, this.state.getValueState());
    }
}
