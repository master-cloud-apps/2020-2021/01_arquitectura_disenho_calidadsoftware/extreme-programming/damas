package org.eyo.damas.views;

import org.eyo.damas.controllers.StartController;
import org.eyo.damas.models.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StartViewTest {

    private StartView startView;
    private State state;
    private Game game;
    private StartController startController;

    @BeforeEach
    public void setUp(){
        this.game = new Game();
        this.state = new State();
        this.startController = new StartController(this.game, this.state);
        this.startView = new StartView();
    }

    @Test
    public void should_fail_when_no_controller_is_passed_to_interact(){
        assertThrows(AssertionError.class, () -> this.startView.interact(null));
    }

    @Test
    public void should_change_state_when_interact_with_start(){
        assertEquals(StateValue.INITIAL, this.state.getValueState());

        this.startView.interact(this.startController);

        assertEquals(StateValue.IN_GAME, this.state.getValueState());
        assertEquals(Color.WHITE, this.startController.getColor(Coordinate.getInstance("g3")));
    }
}
